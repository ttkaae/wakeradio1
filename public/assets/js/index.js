var socket = io.connect(location.origin);
var playBtn = document.getElementById("play-btn");
var pauseBtn = document.getElementById("pause-btn");
var dltTblBtn = document.getElementById("dltTblBtn");
var $timeServer = document.getElementById("timeServer");
var $timeClient = document.getElementById("timeClient");
var $playState = document.getElementById("playState")


var weekdays = ["Sun","Man","Tue","Wed","Thu","Fri","Sat"]; 
var $h = $("#h");
var $m = $("#m");
var $weekdays = $("#weekdays");
var $weekends = $("#weekends");
var $alrmList = $("#alrmList");
var alarm = "";
var player;
var radioStream;
var paused ;

var updVars = function(){
	radioStream = $('#radioStream').find('input[type=radio]:checked').attr("value"); //refresh
	return radioStream
}

playBtn.addEventListener("click", function () {
	updVars()
	socket.emit('play',radioStream);
	paused = false;
	txtPause(paused)
});

pauseBtn.addEventListener("click", function () {
	socket.emit('pause');
	paused = true;
	txtPause(paused)
});

 dltTblBtn.addEventListener("click", function () {
	//Test purpose button
	socket.emit("dlt")
});

$('document').ready(function(){
	socket.emit("ClientLoad");
})

var txtPause = function(p){
	if(!p){
		$playState.textContent = "Playing";
	} else {
		$playState.textContent = "Not playing";
	}
}

socket.on('paused',function(data){
	txtPause(data)
})


socket.on('clock', function(data) {
	//Time on server
	var t = JSON.parse(data);
	var dtServer = new Date (t.time[0].dtServer.toString());
	//console.log(dtServer)
	var hServer = dtServer.getHours();
	var mServer = dtServer.getMinutes();
	var sServer = dtServer.getSeconds();
	
	if(hServer < 10){hServerT = "0"+hServer} else if (hServer == 24) {hServerT = 24 % hServer} else {hServerT = hServer}
	if(mServer < 10){mServerT = "0"+mServer} else {mServerT = mServer}
	if(sServer < 10){sServerT = "0"+sServer} else {sServerT = sServer}
	
	var dayServer = dtServer.getDay();
	
	var tStr = hServerT +':' + mServerT + ':' + sServerT + ': ' + weekdays[dayServer];
	$timeServer.textContent = tStr.toString();

})

var svAlarm = function(){
	var setHour = parseInt($("#h").val());
	var setMin = parseInt($("#m").val());
	updVars()
	
	var alarmObj = {};
	var setAlarm = function(setHour,setMin,setDay){
		alarmObj.hour = setHour;
		alarmObj.min = setMin;
		alarmObj.day = setDay;
		alarmObj.rdSt = radioStream;
		console.log(radioStream);
		
		alarmObj = JSON.stringify(alarmObj);
		return alarmObj;
	}

	if(setHour < 24 && setHour >= 0 && setMin >= 0 && setMin<= 59){
		if(setHour < 10){hourT = "0"+setHour} else {hourT = setHour}
		if(setMin < 10 ){minT = "0"+setMin} else {minT = setMin}
		$h.textContent = "";
		$m.textContent = "";

		if($("#weekdays").is(":checked")){
			setDay = 1; // Like Mondays
			setAlarm(setHour,setMin,setDay)
		} else if($("#weekends").is(":checked")){
			setDay = 0; // Like Sundays
			setAlarm(setHour,setMin,setDay)
		} else if (!$("#weekdays").checked && !$("#weekends").checked ){
			setDay = 2; //all days
			setAlarm(setHour,setMin,setDay)
		} else if ($("#weekdays").checked && $("#weekends").checked ){
			setDay = 2; //all days
			setAlarm(setHour,setMin,setDay)
		} ;
		socket.emit('svAlarmDB',alarmObj);
	} else {
		alert("error in time");
	}
}

socket.on("alrmExists",function(data){
	alrmExists = data;
	if(alrmExists){alert("The alarm already exists!")};
})

socket.on("alarmsDB",function(data){
	console.log(data);
	var alarmsDB= data;
	tblPopulate(alarmsDB)
})

var tblPopulate = function(obj){
	$alrmList.find("tbody > tr").remove();
	var txt = "" 
	
	$.each(obj, function(index, value) {
		var i = parseInt(index)
		if(!isNaN(i)){ //you should have had an obj.alarms instead
			var day = parseInt(obj[i].day)
			switch(day){
				case 0 : txt = "Weekends"
				break;
				
				case 1 : txt = "Weekdays";
				break;
				
				case 2 : txt = "All days"
				break;
				
				default: txt = "All days"
				break;
			}
			
			var dltBtn = '<input type="button" value = "remove" onclick="dltAlrmListItem('+i+')"></input>'
			var onOffBtn = '<input class="onOffBtnOn btn btn-primary btn-sm btn-default" value="on" onclick="onOffAlrm('+i+')" type="button">'
			var txtInsrt = '<tr ><td>'+addZero(obj[i].hour)+':'+addZero(obj[i].min)+'</td><td>'+txt+'</td><td>'+obj[i].rdSt+'</td><td>'+dltBtn+'</td><td>'+onOffBtn+'</td></tr>'
			
			$alrmList.find("tbody").prepend(txtInsrt);			
		}
	});
}

var dltAlrmListItem = function(rowid){
	socket.emit("dltAlrmListItem",rowid);
	console.log("delete this one: " + rowid);
}

var onOffAlrm = function(rowid){
	//socket.emit("onOffAlrm",rowid);
	console.log("On off button")
}

var addZero = function(val){
	if(val < 10){val = "0" + val} else {val = val};
	return val
}

