var vol = 8;
var slider = $('.volSlider');
var socket = io.connect(location.origin);

$(function() {
    slider.slider({
    	range: "min",
    	value: vol,
    });
});

slider.on('slideStop', function(){
	vol = slider.val()
	socket.emit("volume",vol);
});
