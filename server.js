var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var fs = require("fs");
var file = "test.db";
var exists = fs.existsSync(file);

//Accessing sqlite db
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);
var sqlQuery = "";

//checkout howler.js

//Clock
var now; //Global value
var alarmsDB = {};
var wr = {};
var vol = 8;
var paused = true;

app.use(express.static('public'));
server.listen(8080,function(){
	console.log("Listening on *:8080")
});


io.on('connection', function (socket) {
	
    socket.on('play', function(data) {
       socket.broadcast.emit('play',data);
       paused = false
       socket.emit('paused',paused);
    });


    socket.on('pause', function() {
        socket.broadcast.emit('pause');
        paused = true
        socket.emit('paused',paused);
    });
	
	socket.on('ClientLoad',function(){
		wr.objPopulate(true)
		wr.timer()
		socket.emit('paused',paused);
	})
	
    socket.on('volume', function(data) {
    	vol = data
       	socket.broadcast.emit('volume',vol);
       	console.log(vol)
    });
	
	//###

	socket.on("dlt",function(data){
		sqlQuery = "DROP table IF EXISTS alarms";
		db.serialize(function(){
			db.run(sqlQuery);
			console.log("table alarms is dropped");
			wr.objPopulate(true);
		})
	})
	
	//###

	socket.on("dltAlrmListItem",function(data){
		var dltAlrmListItem = parseInt(data);
		console.log(dltAlrmListItem)
		db.serialize(function() {
			sqlQuery = 'DELETE FROM alarms WHERE rowid="'+dltAlrmListItem+'"';
			db.run(sqlQuery);
			//db.close();
			console.log("dltAlrmListItem is deleted")
		})
		wr.objPopulate(true);
	})

	socket.on('svAlarmDB', function(data) {
		var alarm = data
		wr.dbInsert(alarm); //Insert alarm to db
    });
	
});

wr.dbInsert = function(insrt){
	//insrt must be in JSON format
	var alrmExists = true; //Saying that it does exist, prove me wrong
	
	//sqlQuery = 'SELECT * FROM alarms';

	var matchCount = 0;
	db.serialize(function() {
		sqlQuery = 'CREATE TABLE IF NOT EXISTS alarms (JSONdetails TEXT UNIQUE NOT NULL)';
		db.run(sqlQuery);
		var stmt = db.prepare("INSERT INTO alarms VALUES (?)");
		sqlQuery = 'SELECT rowid AS id, JSONdetails FROM alarms';
		db.each(sqlQuery,function(err,row){
			if (insrt == row.JSONdetails){
				alrmExists = true;
				matchCount++
				//return false; //To break the each loop 
			}

		}
		,function(){
			console.log("Callback after dbInsert")
			if(matchCount == 0){
				stmt.run(insrt); //Insert data
				stmt.finalize();
			} else {
				console.log("Alarm already exists")
				io.emit("alrmExists",alrmExists)
			}
			return false; //To break the each loop
		})
		//db.close();
	})
	wr.objPopulate(true);
}

wr.objPopulate = function(send){
	//console.log("objPopulate")
	db.serialize(function() {
		var obj = {};
		sqlQuery = 'SELECT rowid AS id, JSONdetails FROM alarms';
		db.each(sqlQuery,function(err,row){
			JSONdetails = JSON.parse(row.JSONdetails)
			//JSONdetails = row.JSONdetails
			key = parseInt(row.id)
			obj[key] = {
				key : key,
				hour : JSONdetails.hour,
				min : JSONdetails.min,
				day: JSONdetails.day,
				rdSt : JSONdetails.rdSt
			}
			//console.log("now whar: " + alarmsDB[1])
			//alarmsDB[row.id] = row.JSONdetails;
			//console.log("hour: "+obj[key].hour + " key: " + key)
		},function(){
			alarmsDB = obj;
			if(send){
				io.emit("alarmsDB",alarmsDB);
			} else {
				//do not send
			}
			return alarmsDB;
		})
	})
}


wr.prepareRadio = function(){
	setTimeout(function(){
		io.emit('pause');
	},59000)
	console.log("prepareRadio 1 s before new alarm")
}

wr.nxtAlarm = function(nowObj){
	//console.log('nxtAlarm')
	wr.objPopulate()
	//Every minute the next alarm is checked in this function by looking throught object
	var hNow = parseInt(nowObj.getHours());
	var mNow = parseInt(nowObj.getMinutes());
	var dNow = parseInt(nowObj.getDay());
	
	//console.log("obj6 "+alarmsDB[12].hour)
	//console.log("obj5: " + alarmsDB[1].hour)
	//console.log("try this: " + alarmsDB[1].hour)
	
	var hour,min,day,rdSt;
	
	for(var key in alarmsDB) { 
		var i = parseInt(key)
		hour = parseInt(alarmsDB[i].hour)
		min = parseInt(alarmsDB[i].min)
		day = parseInt(alarmsDB[i].day)
		rdSt = alarmsDB[i].rdSt
		if(hour == hNow){
			if (min == mNow){
				console.log("dNow= " + dNow + " day: " + day )
				//console.log("rdSt= " + rdSt)
				switch(true){
					//weekends
					 case (dNow == 0 ||  dNow == 6 && day == 0): wr.alrmPlay(rdSt);
					 break; 
					 
					//weekdays
					 case (dNow > 0 &&  dNow < 6 && day == 1): wr.alrmPlay(rdSt);
					 break; 
					 
					 //all days
					 case (dNow >= 0 && dNow <=6 && day == 2): wr.alrmPlay(rdSt); //console.log("should play here")
					 break; 
				}					
			} else if (min - 1 == mNow){
				wr.prepareRadio(); //The radio.html page must be refreshed just before playing. Otherwise it loses air when playing. but why, hm?
			}
		}
	}
}

wr.clock = function(){
	//console.log("clock function")
	//For some reason this function runs three times every second
	var dtServer = new Date();
	now = JSON.stringify({"time":[{"dtServer":dtServer}]})
	io.emit('clock',now);
	var dSec = parseInt(dtServer.getSeconds())
	if (dSec == 0){
		//console.log("evrybody run 1")
		wr.nxtAlarm(dtServer)
		//make function //if not, it will be true 3-5 times (in a second???)
	}
}

wr.alrmPlay = function(rdSt){
	io.emit("playrdSt",rdSt)
	paused = false
	io.emit('paused',paused);
}

wr.timer = function(){
	setInterval(wr.clock,1000)
}
